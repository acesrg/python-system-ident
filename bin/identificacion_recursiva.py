#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 10 11:23:15 2021

@author: ibrahim
"""
import numpy as np
import threading
import logging
import time
from system_ident_lib.listeners.uart import SerialESP
from system_ident_lib.identification.ARX import RecursiveARX
running = True

def adquisition_loop(pipeline):
    NAME = "acquisition"
    logging.info("Thread %s: starting", NAME)
    keys_to_get = ['pwm', 'adc']
    dict_of_values = {'pwm':[1],'adc':[1]}
    esp = SerialESP()
    while running:
        data = esp.read_json()
        if data:
            for key in keys_to_get:
                dict_of_values[key].append(data[key])
            logging.debug("Data read: %s", dict_of_values)
            pipeline.set_data(dict_of_values, NAME)
        time.sleep(0.04)
    logging.info("Thread %s: finishing", NAME)


def duty_to_voltage(duty):
    MAX_VOLTAGE = 3.3
    return MAX_VOLTAGE * (duty / 100)

def recursive_identification_loop(pipeline):
    NAME = "recursive identification"
    logging.info("Thread %s: starting", NAME)
    time.sleep(1)

    arx = RecursiveARX()

    while running:
        data = pipeline.get_info(NAME)

        u = duty_to_voltage(data['pwm'][-1])
        y = data['adc'][-1]

        arx.add_sample(u, y)

        logging.info("Theta:" + str(arx.get_Theta()))
        time.sleep(0.05)
    logging.info("Thread %s: finishing", NAME)

class Pipeline(object):

    def __init__(self):
        self.data={}
        self.data_lock = threading.Lock()

    def set_data(self, data, name):
        logging.debug("%s:about to acquire setlock", name)
        self.data_lock.acquire()
        logging.debug("%s:have lock", name)
        self.data= data
        logging.debug("%s:about to release lock", name)
        self.data_lock.release()
        logging.debug("%s:lock released", name)

    def get_info(self, name):
        logging.debug("%s:about to acquire getlock", name)
        self.data_lock.acquire()
        logging.debug("%s:have lock", name)
        data = self.data
        logging.debug("%s:about to release lock", name)
        self.data_lock.release()
        logging.debug("%s:lock released", name)
        return data

if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    pipeline = Pipeline()

    logging.info("Main    : create and start threads.")
    lector_thread = threading.Thread(target=adquisition_loop, args=(pipeline,))
    recursive_identification_thread = threading.Thread(target=recursive_identification_loop, args=(pipeline,))

    lector_thread.start()
    recursive_identification_thread.start()

    time.sleep(20)
    running = False

    lector_thread.join()
    recursive_identification_thread.join()
