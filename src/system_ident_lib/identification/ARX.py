import numpy as np


class RecursiveARX:
    def __init__(self, u_prev=0, y_prev=0, P_0=np.diag([1e7, 1e7]), Theta_0=np.zeros([2, 1])):
        """
        """
        self.P = P_0
        self.Theta = Theta_0
        self.u = u_prev
        self.y = y_prev

    @property
    def Phi(self):
        return np.array([[-self.y_prev, self.u_prev]])

    @staticmethod
    def _calculate_K(Phi, P_prev):
        """Implements (2.27)

        Note: Internal method
        """
        unos = np.ones((1,1))
        Phi_i = np.linalg.inv(unos + np.dot(np.dot(Phi,P_prev),Phi.T))
        K = np.dot(np.dot(P_prev,Phi.T),Phi_i)
        return K

    @staticmethod
    def _calculate_P(P_prev, K, Phi):
        """Implements (2.30)

        Note: Internal method
        """
        P = P_prev-np.dot(np.dot(K,Phi),P_prev)
        return P

    @staticmethod
    def _estimate_Theta(Theta_prev, K, y, Phi):
        """Implements (2.28)

        Note: Internal method
        """
        Theta = Theta_prev + np.dot(K,(y - np.dot(Phi,Theta_prev)))
        return Theta

    def _update_states(self):
        """Iterates over a new set of samples.

        Updates K, Theta and P according to new inputs/outputs and former
        values of inputs/outputs K, Theta and P.

        Note: Internal method
        """
        self.K = self._calculate_K(self.Phi, self.P)
        self.Theta = self._estimate_Theta(self.Theta, self.K, self.y, self.Phi)
        self.P = self._calculate_P(self.P, self.K, self.Phi)


    def add_sample(self, u, y):
        """Adds a new set of inputs and outputs, and triggers a state-update.

        Given new inputs (u) and outputs (y) for the modelled process, trigger
        a state-update. That is to say, estimate a new Theta.
        """
        self.u_prev = self.u
        self.y_prev = self.y
        self.u = u
        self.y = y

        self._update_states()

    def get_Theta(self):
        """Getter for Theta.
        """
        return self.Theta