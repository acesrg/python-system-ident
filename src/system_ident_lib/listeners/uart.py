import logging
import serial
import json
from json.decoder import JSONDecodeError


class SerialESP:
    def __init__(self, port='/dev/ttyUSB0', baudrate=115200, bytesize=8, timeout=1):
        self.serial_port = serial.Serial(
            port=port,
            baudrate=baudrate,
            bytesize=bytesize,
            timeout=timeout,
        )

    def read_json(self):
        line = self.serial_port.readline()
        data = ""
        try:
            data = json.loads(line)
        except JSONDecodeError:
            logging.error(f"Error reading line: {line}.will be ignored")
        return data
