#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 10 11:23:15 2021

@author: ibrahim
"""
import serial
import json
from json.decoder import JSONDecodeError
import numpy as np
import threading
import logging
import time

running = True
 
def adquisition_loop(pipeline):
    NAME = "acquisition"
    logging.info("Thread %s: starting", NAME)
    keys_to_get = ['pwm', 'adc']
    dict_of_values = {}
    serialPort = serial.Serial(port = "/dev/ttyUSB0" , baudrate=115200, bytesize=8, timeout=1)  
    dict_of_values = {'pwm':[1],'adc':[1]}
    while running:
        line = serialPort.readline()
        data = ""
        try:
            data = json.loads(line)
            for key in keys_to_get:
                dict_of_values[key].append(data[key])
        except JSONDecodeError:
            print("Error reading line: {}.will be ignored".format(line))
            for key in keys_to_get:
                dict_of_values[key].append(1)
        logging.debug("Thread: %s", NAME)
        logging.debug("Data read: %s", dict_of_values)
        time.sleep(0.04)
        pipeline.set_data(dict_of_values, NAME)
    logging.info("Thread %s: finishing", NAME)


def duty_to_voltage(duty):
    MAX_VOLTAGE = 3.3
    return MAX_VOLTAGE * (duty / 100)

def recursive_identification_loop(pipeline):
    NAME = "recursive identification"
    logging.info("Thread %s: starting", NAME)
    time.sleep(1) 

    arx = RecursiveARX()

    while running:
        data = pipeline.get_info(NAME)

        u = duty_to_voltage(data['pwm'][-1])
        y = data['adc'][-1]

        arx.add_sample(u, y)

        logging.info("Theta:" + str(arx.get_Theta()))
        time.sleep(0.05)
    logging.info("Thread %s: finishing", NAME)


class RecursiveARX:
    def __init__(self, u_prev=0, y_prev=0, P_0=np.diag([1e7, 1e7]), Theta_0=np.zeros([2, 1])):
        """
        """
        self.P = P_0
        self.Theta = Theta_0
        self.u = u_prev
        self.y = y_prev

    @property
    def Phi(self):
        return np.array([[-self.y_prev, self.u_prev]])

    @staticmethod
    def _calculate_K(Phi, P_prev):
        """Implements (2.27)

        Note: Internal method
        """
        unos = np.ones((1,1))
        Phi_i = np.linalg.inv(unos + np.dot(np.dot(Phi,P_prev),Phi.T))
        K = np.dot(np.dot(P_prev,Phi.T),Phi_i)
        return K

    @staticmethod
    def _calculate_P(P_prev, K, Phi):
        """Implements (2.30)

        Note: Internal method
        """
        P = P_prev-np.dot(np.dot(K,Phi),P_prev)
        return P

    @staticmethod
    def _estimate_Theta(Theta_prev, K, y, Phi):
        """Implements (2.28)

        Note: Internal method
        """
        Theta = Theta_prev + np.dot(K,(y - np.dot(Phi,Theta_prev)))
        return Theta

    def _update_states(self):
        """Iterates over a new set of samples.

        Updates K, Theta and P according to new inputs/outputs and former
        values of inputs/outputs K, Theta and P.

        Note: Internal method
        """
        self.K = self._calculate_K(self.Phi, self.P)
        self.Theta = self._estimate_Theta(self.Theta, self.K, self.y, self.Phi)
        self.P = self._calculate_P(self.P, self.K, self.Phi)


    def add_sample(self, u, y):
        """Adds a new set of inputs and outputs, and triggers a state-update.

        Given new inputs (u) and outputs (y) for the modelled process, trigger
        a state-update. That is to say, estimate a new Theta.
        """
        self.u_prev = self.u
        self.y_prev = self.y
        self.u = u
        self.y = y

        self._update_states()

    def get_Theta(self):
        """Getter for Theta.
        """
        return self.Theta

class Pipeline(object):
    
    def __init__(self):
        self.data={}
        self.data_lock = threading.Lock()
        
    def set_data(self, data, name):
        logging.debug("%s:about to acquire setlock", name)
        self.data_lock.acquire()
        logging.debug("%s:have lock", name)
        self.data= data
        logging.debug("%s:about to release lock", name)
        self.data_lock.release()
        logging.debug("%s:lock released", name)
        
    def get_info(self, name):
        logging.debug("%s:about to acquire getlock", name)
        self.data_lock.acquire()
        logging.debug("%s:have lock", name)
        data = self.data
        logging.debug("%s:about to release lock", name)
        self.data_lock.release()
        logging.debug("%s:lock released", name)
        return data

if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    pipeline = Pipeline()
    
    logging.info("Main    : create and start threads.")
    lector_thread = threading.Thread(target=adquisition_loop, args=(pipeline,))
    recursive_identification_thread = threading.Thread(target=recursive_identification_loop, args=(pipeline,))

    lector_thread.start()
    recursive_identification_thread.start()
    
    time.sleep(20)
    running = False
    
    lector_thread.join()
    recursive_identification_thread.join()