import os
from setuptools import setup, find_packages


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="python-system-ident",
    version="0.1.0",
    author="Ibrahim Jaime",
    author_email="ibrahimjaime@gmail.com",
    description=("A filter identifier for control-systems laboratories."),
    license="GPLv3",
    keywords="filter laboratories",
    url="http://packages.python.org/python-system-ident",
    packages=[
        'system_ident_lib.{}'.format(sub_pkg) for sub_pkg in find_packages(
            where='src/system_ident_lib'
        )
    ],
    package_dir={'': 'src'},
    long_description=read('README.md'),
    long_description_content_type='text/markdown',
    install_requires=[
        'pyserial==3.4',
        'numpy==1.16.2',
    ],


)
